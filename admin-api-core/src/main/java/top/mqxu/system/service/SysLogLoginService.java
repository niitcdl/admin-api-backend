package top.mqxu.system.service;

import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.system.entity.SysLogLoginEntity;
import top.mqxu.system.query.SysLogLoginQuery;
import top.mqxu.system.vo.SysLogLoginVO;

/**
 * 登录日志
 *
 * @author moqi
 */
public interface SysLogLoginService extends BaseService<SysLogLoginEntity> {

    /**
     * Page result.
     *
     * @param query the query
     * @return the page result
     */
    PageResult<SysLogLoginVO> page(SysLogLoginQuery query);

    /**
     * 保存登录日志
     *
     * @param username  用户名
     * @param status    登录状态
     * @param operation 操作信息
     */
    void save(String username, Integer status, Integer operation);

    /**
     * 导出登录日志
     */
    void export();
}