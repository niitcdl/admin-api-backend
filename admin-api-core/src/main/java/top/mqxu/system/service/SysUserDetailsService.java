package top.mqxu.system.service;


import top.mqxu.system.entity.SysUserEntity;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * SysUserDetailsService
 *
 * @author moqi
 */
public interface SysUserDetailsService {

    /**
     * 获取 UserDetails 对象
     */
    UserDetails getUserDetails(SysUserEntity userEntity);
}
