package top.mqxu.system.service;

import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.system.entity.SysDictTypeEntity;
import top.mqxu.system.query.SysDictTypeQuery;
import top.mqxu.system.vo.SysDictTypeVO;
import top.mqxu.system.vo.SysDictVO;

import java.util.List;

/**
 * 数据字典
 *
 * @author moqi
 */
public interface SysDictTypeService extends BaseService<SysDictTypeEntity> {

    PageResult<SysDictTypeVO> page(SysDictTypeQuery query);

    void save(SysDictTypeVO vo);

    void update(SysDictTypeVO vo);

    void delete(List<Long> idList);

    /**
     * 获取动态SQL数据
     */
    List<SysDictVO.DictData> getDictSql(Long id);

    /**
     * 获取全部字典列表
     */
    List<SysDictVO> getDictList();

    /**
     * 刷新字典缓存
     */
    void refreshTransCache();

}