package top.mqxu.system.service;


import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.system.entity.SysRoleEntity;
import top.mqxu.system.query.SysRoleQuery;
import top.mqxu.system.vo.SysRoleDataScopeVO;
import top.mqxu.system.vo.SysRoleVO;

import java.util.List;

/**
 * 角色
 *
 * @author moqi
 */
public interface SysRoleService extends BaseService<SysRoleEntity> {

    PageResult<SysRoleVO> page(SysRoleQuery query);

    List<SysRoleVO> getList(SysRoleQuery query);

    void save(SysRoleVO vo);

    void update(SysRoleVO vo);

    void dataScope(SysRoleDataScopeVO vo);

    void delete(List<Long> idList);
}
