package top.mqxu.system.service;


import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.system.entity.SysDictDataEntity;
import top.mqxu.system.query.SysDictDataQuery;
import top.mqxu.system.vo.SysDictDataVO;

import java.util.List;

/**
 * 数据字典
 *
 * @author moqi
 */
public interface SysDictDataService extends BaseService<SysDictDataEntity> {

    PageResult<SysDictDataVO> page(SysDictDataQuery query);

    void save(SysDictDataVO vo);

    void update(SysDictDataVO vo);

    void delete(List<Long> idList);

}