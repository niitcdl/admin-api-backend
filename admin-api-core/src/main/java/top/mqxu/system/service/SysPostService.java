package top.mqxu.system.service;


import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.system.entity.SysPostEntity;
import top.mqxu.system.query.SysPostQuery;
import top.mqxu.system.vo.SysPostVO;

import java.util.List;

/**
 * 岗位管理
 *
 * @author moqi
 */
public interface SysPostService extends BaseService<SysPostEntity> {

    PageResult<SysPostVO> page(SysPostQuery query);

    List<SysPostVO> getList();

    void save(SysPostVO vo);

    void update(SysPostVO vo);

    void delete(List<Long> idList);
}