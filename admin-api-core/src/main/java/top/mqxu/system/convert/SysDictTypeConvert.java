package top.mqxu.system.convert;


import top.mqxu.system.entity.SysDictTypeEntity;
import top.mqxu.system.vo.SysDictTypeVO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author moqi
 */
@Mapper
public interface SysDictTypeConvert {
    SysDictTypeConvert INSTANCE = Mappers.getMapper(SysDictTypeConvert.class);

    SysDictTypeVO convert(SysDictTypeEntity entity);

    SysDictTypeEntity convert(SysDictTypeVO vo);

    List<SysDictTypeVO> convertList(List<SysDictTypeEntity> list);

}
