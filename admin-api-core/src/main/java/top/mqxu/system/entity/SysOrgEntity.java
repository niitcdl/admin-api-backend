package top.mqxu.system.entity;

import top.mqxu.base.mybatis.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 机构管理
 *
 * @author mqxu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_org")
public class SysOrgEntity extends BaseEntity {
    /**
     * 上级ID
     */
    private Long pid;
    /**
     * 机构名称
     */
    private String name;
    /**
     * 机构描述
     */
    private String remark;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 上级机构名称
     */
    @TableField(exist = false)
    private String parentName;
}