package top.mqxu.system.dao;

import top.mqxu.base.mybatis.dao.BaseDao;
import top.mqxu.system.entity.SysPostEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 岗位管理
 *
 * @author mqxu
 */
@Mapper
public interface SysPostDao extends BaseDao<SysPostEntity> {

}