package top.mqxu.system.dao;

import top.mqxu.base.mybatis.dao.BaseDao;
import top.mqxu.system.entity.SysLogLoginEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 登录日志
 *
 * @author mqxu
 */
@Mapper
public interface SysLogLoginDao extends BaseDao<SysLogLoginEntity> {

}