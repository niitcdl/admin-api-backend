package top.mqxu.system.dao;

import top.mqxu.base.mybatis.dao.BaseDao;
import top.mqxu.system.entity.SysDictTypeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典类型
 *
 * @author mqxu
 */
@Mapper
public interface SysDictTypeDao extends BaseDao<SysDictTypeEntity> {

}
