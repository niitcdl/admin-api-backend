package top.mqxu.base.common.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Swagger配置
 *
 * @author moqi
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi userApi() {
        String[] paths = {"/**"};
        String[] packagedToMatch = {"top.mqxu"};
        return GroupedOpenApi.builder().group("smart-community-admin-api")
                .pathsToMatch(paths)
                .packagesToScan(packagedToMatch).build();
    }

    @Bean
    public OpenAPI customOpenApi() {
        Contact contact = new Contact();
        contact.setName("mqxu@gmail.com");

        return new OpenAPI().info(new Info()
                .title("智慧社区后台管理平台接口")
                .description("智慧社区后台管理平台接口")
                .contact(contact)
                .version("1.0"));
    }

}