package top.mqxu.base.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 基础服务接口
 *
 * @author moqi
 */
public interface BaseService<T> extends IService<T> {

}