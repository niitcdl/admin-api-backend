package top.mqxu.base.mybatis.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 基础Dao
 *
 * @author moqi
 */
public interface BaseDao<T> extends BaseMapper<T> {

}
