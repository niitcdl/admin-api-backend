package top.mqxu.storage.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.mqxu.storage.enums.StorageTypeEnum;
import top.mqxu.storage.properties.StorageProperties;
import top.mqxu.storage.service.AliyunStorageService;
import top.mqxu.storage.service.LocalStorageService;
import top.mqxu.storage.service.MinioStorageService;
import top.mqxu.storage.service.StorageService;

/**
 * 存储配置文件
 *
 * @author mqxu
 */
@Configuration
@EnableConfigurationProperties(StorageProperties.class)
@ConditionalOnProperty(prefix = "storage", value = "enabled")
public class StorageConfiguration {

    @Bean
    public StorageService storageService(StorageProperties properties) {
        if (properties.getConfig().getType() == StorageTypeEnum.LOCAL) {
            return new LocalStorageService(properties);
        } else if (properties.getConfig().getType() == StorageTypeEnum.ALIYUN) {
            return new AliyunStorageService(properties);
        } else if (properties.getConfig().getType() == StorageTypeEnum.MINIO) {
            return new MinioStorageService(properties);
        }
        return null;
    }
}
