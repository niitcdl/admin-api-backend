package top.mqxu.storage.enums;

/**
 * 存储类型枚举
 *
 * @author mqxu
 */
public enum StorageTypeEnum {
    /**
     * 本地
     */
    LOCAL,
    /**
     * 阿里云
     */
    ALIYUN,
    /**
     * Minio
     */
    MINIO;
}
