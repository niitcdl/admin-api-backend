package top.mqxu.sms.convert;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import top.mqxu.sms.entity.SmsLogEntity;
import top.mqxu.sms.vo.SmsLogVO;

import java.util.List;

/**
 * 短信日志
 *
 * @author moqi
 */
@Mapper
public interface SmsLogConvert {
    SmsLogConvert INSTANCE = Mappers.getMapper(SmsLogConvert.class);

    SmsLogVO convert(SmsLogEntity entity);

    List<SmsLogVO> convertList(List<SmsLogEntity> list);

}