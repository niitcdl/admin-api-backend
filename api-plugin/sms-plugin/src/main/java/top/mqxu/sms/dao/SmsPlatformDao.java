package top.mqxu.sms.dao;

import org.apache.ibatis.annotations.Mapper;
import top.mqxu.base.mybatis.dao.BaseDao;
import top.mqxu.sms.entity.SmsPlatformEntity;

/**
* 短信平台
*
* @author mqxu
*/
@Mapper
public interface SmsPlatformDao extends BaseDao<SmsPlatformEntity> {

}