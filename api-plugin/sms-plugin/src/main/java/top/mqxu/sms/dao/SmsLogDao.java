package top.mqxu.sms.dao;

import org.apache.ibatis.annotations.Mapper;
import top.mqxu.base.mybatis.dao.BaseDao;
import top.mqxu.sms.entity.SmsLogEntity;

/**
 * 短信日志
 *
 * @author mqxu
 */
@Mapper
public interface SmsLogDao extends BaseDao<SmsLogEntity> {

}