package top.mqxu.sms.sms;


import top.mqxu.base.common.exception.ServerException;
import top.mqxu.sms.enums.SmsPlatformEnum;
import top.mqxu.sms.sms.config.SmsConfig;

import java.util.Map;

/**
 * 短信 Context
 *
 * @author mqxu
 */
public class SmsContext {
    private final SmsStrategy smsStrategy;

    public SmsContext(SmsConfig config) {
        if (config.getPlatform() == SmsPlatformEnum.ALIYUN.getValue()) {
            this.smsStrategy = new AliyunSmsStrategy(config);
        } else {
            throw new ServerException("未知的短信平台");
        }
    }

    public void send(String mobile, Map<String, String> params) {
        smsStrategy.send(mobile, params);
    }
}
