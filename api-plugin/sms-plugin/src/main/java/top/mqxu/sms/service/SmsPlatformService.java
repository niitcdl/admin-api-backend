package top.mqxu.sms.service;

import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.sms.entity.SmsPlatformEntity;
import top.mqxu.sms.query.SmsPlatformQuery;
import top.mqxu.sms.sms.config.SmsConfig;
import top.mqxu.sms.vo.SmsPlatformVO;

import java.util.List;

/**
 * 短信平台
 *
 * @author mqxu
 */
public interface SmsPlatformService extends BaseService<SmsPlatformEntity> {

    PageResult<SmsPlatformVO> page(SmsPlatformQuery query);

    /**
     * 启用的短信平台列表
     */
    List<SmsConfig> listByEnable();

    void save(SmsPlatformVO vo);

    void update(SmsPlatformVO vo);

    void delete(List<Long> idList);

}