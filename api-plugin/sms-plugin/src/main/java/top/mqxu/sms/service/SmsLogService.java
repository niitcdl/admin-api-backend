package top.mqxu.sms.service;


import top.mqxu.base.common.utils.PageResult;
import top.mqxu.base.mybatis.service.BaseService;
import top.mqxu.sms.entity.SmsLogEntity;
import top.mqxu.sms.query.SmsLogQuery;
import top.mqxu.sms.vo.SmsLogVO;

/**
 * 短信日志
 *
 * @author mqxu
 */
public interface SmsLogService extends BaseService<SmsLogEntity> {

    PageResult<SmsLogVO> page(SmsLogQuery query);

}