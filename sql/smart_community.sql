/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : smart_community

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 22/05/2023 17:39:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sms_log
-- ----------------------------
DROP TABLE IF EXISTS `sms_log`;
CREATE TABLE `sms_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `platform_id` bigint DEFAULT NULL COMMENT '平台ID',
  `platform` tinyint DEFAULT NULL COMMENT '平台类型',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `params` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '参数',
  `status` tinyint DEFAULT NULL COMMENT '状态  0：失败   1：成功',
  `error` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='短信日志';

-- ----------------------------
-- Records of sms_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sms_platform
-- ----------------------------
DROP TABLE IF EXISTS `sms_platform`;
CREATE TABLE `sms_platform` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `platform` tinyint DEFAULT NULL COMMENT '平台类型  0：阿里云   1：腾讯云   2：七牛云   3：华为云',
  `sign_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '短信签名',
  `template_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '短信模板',
  `app_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '短信应用ID，如：腾讯云等',
  `sender_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '腾讯云国际短信、华为云等需要',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接入地址，如：华为云',
  `access_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'AccessKey',
  `secret_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'SecretKey',
  `status` tinyint DEFAULT NULL COMMENT '状态  0：禁用   1：启用',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='短信平台';

-- ----------------------------
-- Records of sms_platform
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dict_type_id` bigint NOT NULL COMMENT '字典类型ID',
  `dict_label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典标签',
  `dict_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '字典值',
  `label_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '标签样式',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT NULL COMMENT '排序',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典数据';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_data` VALUES (1, 1, '停用', '0', 'danger', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (2, 1, '正常', '1', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (3, 2, '男', '0', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (4, 2, '女', '1', 'success', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (5, 2, '未知', '2', 'warning', '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (6, 3, '正常', '1', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (7, 3, '停用', '0', 'danger', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (8, 4, '全部数据', '0', '', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (9, 4, '本机构及子机构数据', '1', '', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (10, 4, '本机构数据', '2', '', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (11, 4, '本人数据', '3', '', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (12, 4, '自定义数据', '4', '', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (13, 5, '禁用', '0', 'danger', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (14, 5, '启用', '1', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (15, 6, '失败', '0', 'danger', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (16, 6, '成功', '1', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (17, 7, '登录成功', '0', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (18, 7, '退出成功', '1', 'primary', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (19, 7, '验证码错误', '2', 'warning', '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (20, 7, '账号密码错误', '3', 'danger', '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (21, 8, '否', '0', 'primary', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (22, 8, '是', '1', 'danger', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (23, 9, '是', '1', 'danger', '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (24, 9, '否', '0', 'primary', '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_data` VALUES (25, 10, '阿里云', '0', '', '', 0, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_dict_data` VALUES (26, 10, '腾讯云', '1', '', '', 1, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_dict_data` VALUES (27, 10, '七牛云', '2', '', '', 2, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_dict_data` VALUES (29, 11, '默认', 'default', '', '', 0, 0, 10000, '2023-05-13 19:46:37', 10000, '2023-05-13 19:46:37');
INSERT INTO `sys_dict_data` VALUES (30, 11, '系统', 'system', '', '', 1, 0, 10000, '2023-05-13 19:46:37', 10000, '2023-05-13 19:46:37');
INSERT INTO `sys_dict_data` VALUES (31, 12, '暂停', '0', 'danger', '', 0, 0, 10000, '2023-05-13 19:46:37', 10000, '2023-05-13 19:46:37');
INSERT INTO `sys_dict_data` VALUES (32, 12, '正常', '1', 'primary', '', 1, 0, 10000, '2023-05-13 19:46:37', 10000, '2023-05-13 19:46:37');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典类型',
  `dict_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典名称',
  `dict_source` tinyint DEFAULT '0' COMMENT '来源  0：字典数据  1：动态SQL',
  `dict_sql` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '动态SQL',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT NULL COMMENT '排序',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典类型';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_type` VALUES (1, 'post_status', '状态', 0, NULL, '岗位管理', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (2, 'user_gender', '性别', 0, NULL, '用户管理', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (3, 'user_status', '状态', 0, NULL, '用户管理', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (4, 'role_data_scope', '数据范围', 0, NULL, '角色管理', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (5, 'enable_disable', '状态', 0, NULL, '功能状态：启用 | 禁用 ', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (6, 'success_fail', '状态', 0, NULL, '操作状态：成功 | 失败', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (7, 'login_operation', '操作信息', 0, NULL, '登录管理', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (8, 'params_type', '系统参数', 0, NULL, '参数管理', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (9, 'user_super_admin', '用户是否是超管', 0, NULL, '用户是否是超管', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_dict_type` VALUES (10, 'sms_platform', '平台类型', 0, NULL, '短信管理', 0, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_dict_type` VALUES (11, 'schedule_group', '任务组名', 0, NULL, '定时任务', 0, 0, 10000, '2023-05-13 19:46:37', 10000, '2023-05-13 19:46:37');
INSERT INTO `sys_dict_type` VALUES (12, 'schedule_status', '状态', 0, NULL, '定时任务', 0, 0, 10000, '2023-05-13 19:46:37', 10000, '2023-05-13 19:46:37');
COMMIT;

-- ----------------------------
-- Table structure for sys_log_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_login`;
CREATE TABLE `sys_log_login` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户名',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '登录IP',
  `address` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '登录地点',
  `user_agent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'User Agent',
  `status` tinyint DEFAULT NULL COMMENT '登录状态  0：失败   1：成功',
  `operation` tinyint unsigned DEFAULT NULL COMMENT '操作信息   0：登录成功   1：退出成功  2：验证码错误  3：账号密码错误',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='登录日志';

-- ----------------------------
-- Records of sys_log_login
-- ----------------------------
BEGIN;
INSERT INTO `sys_log_login` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 1, 1, '2023-05-22 17:04:41');
INSERT INTO `sys_log_login` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 1, 0, '2023-05-22 17:04:45');
INSERT INTO `sys_log_login` VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 1, 1, '2023-05-22 17:12:09');
INSERT INTO `sys_log_login` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 1, 0, '2023-05-22 17:12:11');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` bigint DEFAULT NULL COMMENT '上级ID，一级菜单为0',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜单URL',
  `authority` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '授权标识(多个用逗号分隔，如：sys:menu:list,sys:menu:save)',
  `type` tinyint DEFAULT NULL COMMENT '类型   0：菜单   1：按钮   2：接口',
  `open_style` tinyint DEFAULT NULL COMMENT '打开方式   0：内部   1：外部',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜单图标',
  `sort` int DEFAULT NULL COMMENT '排序',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, 0, '系统设置', NULL, NULL, 0, 0, 'icon-setting', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (2, 1, '菜单管理', 'sys/menu/index', NULL, 0, 0, 'icon-menu', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (3, 2, '查看', '', 'sys:menu:list', 1, 0, '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (4, 2, '新增', '', 'sys:menu:save', 1, 0, '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (5, 2, '修改', '', 'sys:menu:update,sys:menu:info', 1, 0, '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (6, 2, '删除', '', 'sys:menu:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (7, 1, '数据字典', 'sys/dict/type', '', 0, 0, 'icon-insertrowabove', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (8, 7, '查询', '', 'sys:dict:page', 1, 0, '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (9, 7, '新增', '', 'sys:dict:save', 1, 0, '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (10, 7, '修改', '', 'sys:dict:update,sys:dict:info', 1, 0, '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (11, 7, '删除', '', 'sys:dict:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (12, 0, '权限管理', '', '', 0, 0, 'icon-safetycertificate', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (13, 12, '岗位管理', 'sys/post/index', '', 0, 0, 'icon-solution', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (14, 13, '查询', '', 'sys:post:page', 1, 0, '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (15, 13, '新增', '', 'sys:post:save', 1, 0, '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (16, 13, '修改', '', 'sys:post:update,sys:post:info', 1, 0, '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (17, 13, '删除', '', 'sys:post:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (18, 12, '机构管理', 'sys/org/index', '', 0, 0, 'icon-cluster', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (19, 18, '查询', '', 'sys:org:list', 1, 0, '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (20, 18, '新增', '', 'sys:org:save', 1, 0, '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (21, 18, '修改', '', 'sys:org:update,sys:org:info', 1, 0, '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (22, 18, '删除', '', 'sys:org:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (23, 12, '角色管理', 'sys/role/index', '', 0, 0, 'icon-team', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (24, 23, '查询', '', 'sys:role:page', 1, 0, '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (25, 23, '新增', '', 'sys:role:save,sys:role:menu,sys:org:list', 1, 0, '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (26, 23, '修改', '', 'sys:role:update,sys:role:info,sys:role:menu,sys:org:list,sys:user:page', 1, 0, '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (27, 23, '删除', '', 'sys:role:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (28, 12, '用户管理', 'sys/user/index', '', 0, 0, 'icon-user', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (29, 28, '查询', '', 'sys:user:page', 1, 0, '', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (30, 28, '新增', '', 'sys:user:save,sys:role:list', 1, 0, '', 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (31, 28, '修改', '', 'sys:user:update,sys:user:info,sys:role:list', 1, 0, '', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (32, 28, '删除', '', 'sys:user:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (33, 0, '应用管理', '', '', 0, 0, 'icon-appstore', 2, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (34, 0, '日志管理', '', '', 0, 0, 'icon-filedone', 3, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (35, 34, '登录日志', 'sys/log/login', 'sys:log:login', 0, 0, 'icon-solution', 0, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (36, 35, '导入', '', 'sys:user:import', 1, 0, '', 5, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (37, 35, '导出', '', 'sys:user:export', 1, 0, '', 6, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (38, 1, '接口文档', '{{apiUrl}}/doc.html', NULL, 0, 1, 'icon-file-text-fill', 10, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
INSERT INTO `sys_menu` VALUES (39, 33, '短信管理', '', '', 0, 0, 'icon-message', 2, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-22 17:19:24');
INSERT INTO `sys_menu` VALUES (40, 39, '短信日志', 'message/sms/log/index', 'sms:log', 0, 0, 'icon-detail', 1, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_menu` VALUES (41, 39, '短信平台', 'message/sms/platform/index', NULL, 0, 0, 'icon-whatsapp', 0, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_menu` VALUES (42, 41, '查看', '', 'sms:platform:page', 1, 0, '', 0, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_menu` VALUES (43, 41, '新增', '', 'sms:platform:save', 1, 0, '', 1, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_menu` VALUES (44, 41, '修改', '', 'sms:platform:update,sms:platform:info', 1, 0, '', 2, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_menu` VALUES (45, 41, '删除', '', 'sms:platform:delete', 1, 0, '', 3, 0, 10000, '2023-05-13 19:46:21', 10000, '2023-05-13 19:46:21');
INSERT INTO `sys_menu` VALUES (64, 0, '缴费管理', '', '', 0, 0, '', 0, 0, 10000, '2023-05-22 17:35:53', 10000, '2023-05-22 17:35:53');
INSERT INTO `sys_menu` VALUES (65, 64, '收费项目', 'payment/index', '', 0, 0, 'icon-detail', 0, 0, 10000, '2023-05-22 17:36:24', 10000, '2023-05-22 17:36:24');
INSERT INTO `sys_menu` VALUES (66, 64, '押金管理', 'deposit/index', '', 0, 0, 'icon-wallet', 1, 0, 10000, '2023-05-22 17:36:50', 10000, '2023-05-22 17:36:50');
COMMIT;

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` bigint DEFAULT NULL COMMENT '上级ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '机构名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '机构作用说明',
  `sort` int DEFAULT NULL COMMENT '排序',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='机构管理';

-- ----------------------------
-- Records of sys_org
-- ----------------------------
BEGIN;
INSERT INTO `sys_org` VALUES (1, 0, '财务部', '财务部', 0, 0, 10000, '2023-05-22 17:34:40', 10000, '2023-05-22 17:34:40');
INSERT INTO `sys_org` VALUES (2, 0, '客服部', '客服部', 1, 0, 10000, '2023-05-22 17:34:50', 10000, '2023-05-22 17:34:50');
COMMIT;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `post_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '岗位编码',
  `post_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '岗位名称',
  `sort` int DEFAULT NULL COMMENT '排序',
  `status` tinyint DEFAULT NULL COMMENT '状态  0：停用   1：正常',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='岗位管理';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  `data_scope` tinyint DEFAULT NULL COMMENT '数据范围  0：全部数据  1：本机构及子机构数据  2：本机构数据  3：本人数据  4：自定义数据',
  `org_id` bigint DEFAULT NULL COMMENT '机构ID',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_org_id` (`org_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色管理';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '平台运营人员', '平台运营人员', 0, NULL, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role` VALUES (2, '财务部负责人', '财务部负责人', 1, NULL, 0, 10000, '2023-05-22 17:37:06', 10000, '2023-05-22 17:37:06');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_data_scope`;
CREATE TABLE `sys_role_data_scope` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `org_id` bigint DEFAULT NULL COMMENT '机构ID',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色数据权限';

-- ----------------------------
-- Records of sys_role_data_scope
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint DEFAULT NULL COMMENT '菜单ID',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_role_id` (`role_id`) USING BTREE,
  KEY `idx_menu_id` (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色菜单关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (1, 1, 12, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (2, 1, 28, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (3, 1, 29, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (4, 1, 30, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (5, 1, 31, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (6, 1, 32, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (7, 1, 18, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (8, 1, 19, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (9, 1, 20, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (10, 1, 21, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (11, 1, 22, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (12, 1, 13, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (13, 1, 14, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (14, 1, 15, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (15, 1, 16, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (16, 1, 17, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (17, 1, 23, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (18, 1, 24, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (19, 1, 25, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (20, 1, 26, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (21, 1, 27, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (22, 1, 1, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (23, 1, 2, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (24, 1, 3, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (25, 1, 4, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (26, 1, 5, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (27, 1, 6, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (28, 1, 7, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (29, 1, 8, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (30, 1, 10, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (31, 1, 9, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (32, 1, 11, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (33, 1, 38, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (34, 1, 33, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (35, 1, 39, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (36, 1, 41, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (37, 1, 42, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (38, 1, 43, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (39, 1, 44, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (40, 1, 45, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (41, 1, 40, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (42, 1, 34, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (43, 1, 35, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (44, 1, 36, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (45, 1, 37, 0, 10000, '2023-05-22 17:33:45', 10000, '2023-05-22 17:33:45');
INSERT INTO `sys_role_menu` VALUES (46, 2, 64, 0, 10000, '2023-05-22 17:37:06', 10000, '2023-05-22 17:37:06');
INSERT INTO `sys_role_menu` VALUES (47, 2, 65, 0, 10000, '2023-05-22 17:37:06', 10000, '2023-05-22 17:37:06');
INSERT INTO `sys_role_menu` VALUES (48, 2, 66, 0, 10000, '2023-05-22 17:37:06', 10000, '2023-05-22 17:37:06');
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '密码',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '姓名',
  `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '头像',
  `gender` tinyint DEFAULT NULL COMMENT '性别   0：男   1：女   2：未知',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '手机号',
  `org_id` bigint DEFAULT NULL COMMENT '机构ID',
  `super_admin` tinyint DEFAULT NULL COMMENT '超级管理员   0：否   1：是',
  `status` tinyint DEFAULT NULL COMMENT '状态  0：停用   1：正常',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户管理';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (10000, 'admin', '{bcrypt}$2a$10$zJunDEdPkJuzVuTfO.jmxeNX/izaxMrBWeJIJKMoecxZ6EV3QQ03C', '超级管理员', 'https://niit-soft.oss-cn-hangzhou.aliyuncs.com/avatar/me.png', 0, 'admin@gmail.com', '13951905171', NULL, 1, 1, 0, 10000, '2023-05-13 19:37:15', 10000, '2023-05-13 19:37:15');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint DEFAULT NULL COMMENT '用户ID',
  `post_id` bigint DEFAULT NULL COMMENT '岗位ID',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_user_id` (`user_id`) USING BTREE,
  KEY `idx_post_id` (`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户岗位关系';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `user_id` bigint DEFAULT NULL COMMENT '用户ID',
  `deleted` tinyint DEFAULT NULL COMMENT '删除标识  0：正常   1：已删除',
  `creator` bigint DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_role_id` (`role_id`) USING BTREE,
  KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户角色关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
